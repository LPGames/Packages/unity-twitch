using System.Collections.Generic;
using UnityEngine;

namespace UnityTwitch
{
    public class CommandHandler: MonoBehaviour
    {
        private interface ICommand
        {
            public string CommandName { get; }
            public void Execute(string user, string message);
        }

        private List<ICommand> _enabledCommands = new List<ICommand>();

        private void OnReceived(string user, string message)
        {
            bool isCommand = GetCommandFromMessage(message, out ICommand command);
            if (isCommand)
            {
                command.Execute(user, message);
            }
        }

        private bool GetCommandFromMessage(string message, out ICommand foundCommand)
        {
            foundCommand = null;
            foreach (ICommand command in _enabledCommands)
            {
                if (message.Contains(command.CommandName))
                {
                    foundCommand = command;
                    return true;
                }
            }

            return false;
        }

        #region Setup
        private void OnEnable()
        {
            TwitchConnect.OnMessageReceived += OnReceived;
        }


        private void OnDisable()
        {
            TwitchConnect.OnMessageReceived -= OnReceived;
        }
        #endregion
    }
}
